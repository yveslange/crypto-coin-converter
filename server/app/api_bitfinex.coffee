log = require("./logger")
Got = require("got")

IS = "BITFINEX>"

module.exports = class Bitfinex
  config: {
    url: "https://api.bitfinex.com/v2/"
  }

  constructor: (config) ->
    Object.assign(@config, config)
    log.debug IS, "configuration:", @config

  get: (endpoint="") ->
    url = @config.url+endpoint
    return Got("#{url}", {json: true})
    .then( (resp) -> return resp.body)
    .catch( (err) =>
      log.error IS, "ERROR #{err.statusCode} while trying to get url=#{url}", err
    )

  getPlatformStatus: () =>
    return @get("platform/status")

  getTickers: (symbols) =>
    tickers = await @get("tickers?symbols="+symbols.join(","))
    tickersObj = {}
    tickers.forEach( (ticker) =>
      tickersObj[ticker[0]] = {
        symbol: ticker[0],
        bid: ticker[1],
        bid_size: ticker[2],
        ask: ticker[3],
        ask_size: ticker[4],
        last_price: ticker[7]
      }
    )
    return tickersObj

