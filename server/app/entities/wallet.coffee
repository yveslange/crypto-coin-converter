Log = require("../logger")

Entity = require("./entity")
Transaction = require("./transaction")

module.exports = class Wallet extends Entity

  constructor: (wallet) ->
    super({
      name: null
      currency: null
      description: null
    }, wallet)
    @transactions = []

  getTransactions: ->
    return @transactions

  addTransaction: (transaction) ->
    if !transaction?
      Log.error "Adding an empty transaction to #{@toString()}"
      return false

    Log.debug "Adding", transaction.toString(), "to", @toString()
    @transactions.push transaction

    return true

  deposit: ( amount, fee) ->
    @addTransaction( new Transaction {
      name: "Deposit"
      amount: amount
      fee: fee
      currency: @variables.currency
    })

  withdraw: ( amount, fee) ->
    @addTransaction( new Transaction {
      name: "Withdraw"
      amount: amount
      fee: fee
      currency: @variables.currency
    })

  # TODO: check the fee
  transfer: ( amount, fee, fx, toWallet) ->
    name = "Transfer from #{@get('name')} to #{toWallet.get('name')}" 
    @addTransaction( new Transaction {
      name: name
      amount: -1 * amount
      fee: -1 * fee
      currency: @variables.currency
      meta: 
        toWallet: toWallet
    })

    toWallet.addTransaction( new Transaction {
      name: name
      amount: (amount - fee) * fx
      currency: @variables.currency
      fee: 0
      fx: fx
      meta:
        fromWallet: @
    })

  getBalance: ->
    @transactions.reduce( (sum, transaction) ->
      return sum + transaction.get('amount')
    , 0)

  getDeposits: ->
    @transactions.reduce( (sum, transaction) -> 
      if transaction.get('type') == 'deposit'
        return sum + transaction.get('amount')
      return sum
    , 0)

  getProfit: ->
    @transactions.reduce( (sum, transaction) -> 
      return sum + transaction.get('amount') - transaction.get("fee")
    , 0)