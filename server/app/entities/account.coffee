Log = require("../logger")

Entity = require("./entity")

module.exports = class Wallet extends Entity
  constructor: (account) ->
    super({
      name: null
      description: null
    }, account)
    
    @wallets = []

  addWallet: (wallet) ->
    @wallets.push wallet
    return wallet