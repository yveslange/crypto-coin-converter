Log = require("../logger")
CryptoHelper = require("../helpers/crypto")

module.exports = class Entity
  constructor: (struct, variables) ->
    @variables = {
      name: null
      meta: 
        _id: CryptoHelper.getSHA256Object( variables)
        timestamp: null
    }
    Object.assign(@variables, struct)
    
    fieldsOK = @validateFields(variables)
    if fieldsOK
      Object.assign( @variables, variables)
    else
      throw new Error("#{@constructor.name}: can't construct Entity with "+JSON.stringify( variables))
    
  # Returns the string version of the class
  toString: ->
    return "#{@constructor.name}: #{ JSON.stringify( @variables)}"

  # Returns the variables or (if specified) a specific field
  get: (field) ->
    if field?
      return @variables[field]
    return @variables

  # Checks that all field of the transaction have been defined
  validateFields: ( variables) ->
    for field, val of @variables when field != 'meta'
      if !variables[field]?
        Log.error "Forgot to specify field '#{field}' for transaction", variables
        return false
    return true