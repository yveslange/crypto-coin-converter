Log = require("../logger")

Entity = require("./entity")

module.exports = class User extends Entity
  constructor: (user) ->
    super({
      name: null
    }, user)
    @accounts = []

  addAccount: (account) ->
    @accounts.push account
    return account