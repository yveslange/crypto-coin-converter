IS = "ROUTER>"
log = require("./logger")
Express = require("express")()
Bitfinex = require("./api_bitfinex")
Currencies = require("./currencies")




buy = (amount, ticker) ->
  money1 = ticker.symbol[1..][0..2]
  money2 = ticker.symbol[1..][3..]
  newAmount = amount/ticker.bid
  return {
    amount: newAmount
    currency: money1
    humain: "Buying #{newAmount} #{money1} for the price of #{amount} #{money2}"
  }

sell = (amount, ticker) ->
  money1 = ticker.symbol[1..][0..2]
  money2 = ticker.symbol[1..][3..]
  newAmount = amount*ticker.ask
  return {
    fromCurrency: money1
    amount: newAmount
    toCurrency: money2
    humain: "Selling #{amount} #{money1} for the price of #{newAmount} #{money2}"
  }

module.exports = class Router
  config: {
    host: "0.0.0.0"
    port: 8080
  }

  constructor: (config) ->
    Object.assign(@config, config)

    log.debug IS, "Listening GET /convert"
    currencies = new Currencies()
    Express.use((req, res, next) ->
      res.header("Access-Control-Allow-Origin", "*")
      res.header("Access-Control-Allow-Headers", "X-Requested-With")
      next()
    )

    Express.get("/convert", (req, res) ->
      amount = req.query.amount
      fsym = req.query.fsym
      tsym = req.query.tsym
      log.info "Asked to convert #{amount} of #{fsym} in #{tsym}"
      result = currencies.convert(amount, fsym, tsym)
      res.json({
        amount: amount,
        fsym: fsym,
        tsym: tsym,
        result: result
      })
    )

    log.debug IS, "Listening GET /"
    Express.get("/", (req, res) ->
      bitfinex = new Bitfinex()
      platformStatus = await bitfinex.getPlatformStatus()
      if !platformStatus
        log.error IS, "Bitfinex platform is in MAINTENANCE"
      else
        log.info IS, "Bitfinex platform is ONLINE"

      tickers = await bitfinex.getTickers(["tIOTBTC", "tIOTUSD", "tBTCUSD", "tETHUSD", "tLTCUSD"])
      log.info tickers

      # USD -> BTC -> IOTA
      myUSD = 100 # dollars
      log.debug( "Initial amount of", myUSD, "USD")

      # Buying some BTC from USD
      myBTC = myUSD/tickers['tBTCUSD'].bid
      log.debug( buy(myUSD, tickers['tBTCUSD']).humain)

      # Buying some IOTA from BTC
      myIOTA = myBTC/tickers['tIOTBTC'].bid
      log.debug( buy(myBTC, tickers['tIOTBTC']).humain)

      # Selling some IOTA to USD
      myNewUSD = myIOTA*tickers['tIOTUSD'].ask
      log.debug( sell(myIOTA, tickers['tIOTUSD']).humain)


      myBTC2 = myIOTA*tickers['tIOTBTC'].ask
      myUSD2 = myBTC2*tickers['tBTCUSD'].ask
      log.debug "Should be the same IOT -> BTC -> USD", myUSD2, "$"

      # Wallets
      kursionWallets = [
        sell(144.26, tickers['tIOTUSD'])
        sell(0.0028538, tickers['tBTCUSD'])
        sell(0.06332744, tickers['tETHUSD'])
        sell(1.8801869, tickers['tLTCUSD'])
      ]
      kursionTotalUSD = 0
      kursionWallets.forEach( (wallet) =>
        kursionTotalUSD += wallet.amount
      )
      log.info "Kursion Wallets", kursionWallets
      log.info "Kursion TOTAL USD", kursionTotalUSD , kursionTotalUSD-504
      log.info "N", sell(184, tickers['tIOTUSD']).amount-355.48
      res.json({ wallet: kursionWallets, kursionTotalUSD: kursionTotalUSD-504})
    )

    Express.listen(@config.port, @config.host, =>
      log.info IS, "server listening on host=#{@config.host} port=#{@config.port}"
    )


