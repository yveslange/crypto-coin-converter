Crypto = require('crypto')

module.exports = {
  getSHA256: (secret) ->
    return Crypto.createHash('sha256').update(secret).digest('hex');

  getSHA256Object: (object) ->
    return @getSHA256( JSON.stringify(object))
}