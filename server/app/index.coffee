
Log = require("./logger")

Router = require("./router")

User = require("./entities/user")
Account = require("./entities/account")
Wallet = require("./entities/wallet")
Transaction = require("./entities/transaction")

IS = "WALLET>"

main = (() ->

  router = new Router()

  # Creating a user
  user = new User({
    name: "kursion"
  })

  # Creating UBS account & wallets
  accountUBS = user.addAccount new Account({
    name: "UBS Bank"
    description: "My personal Bank account"
  })
  walletUBS = accountUBS.addWallet new Wallet({
    name: "Personal Account (CHF)"
    currency: "CHF"
    description: "My personal UBS wallet in CHF"
  })
  walletUBSVisa = accountUBS.addWallet new Wallet({
    name: "UBS Visa (CHF)"
    currency: "CHF"
    description: "My UBS VISA account in CHF"
  })

  # Creating Coinbase account & wallets
  accountCoinbase = user.addAccount new Account({
    name: "Coinbase account"
    description: "Plateforme to exchange crypto-currencies versus real money"
  })
  walletCoinbaseEUR = accountCoinbase.addWallet new Wallet({
    name: "Wallet Coinbase (EUR)"
    currency: "EUR"
    description: "Wallet Coinbase in EUR"
  })
  walletCoinbaseLTC = accountCoinbase.addWallet new Wallet({
    name: "Wallet Coinbase (LTC)"
    currency: "LTC"
    description: "Wallet Coinbase in LTC"
  })

  # Transfer from Bank to Coinbase
  walletUBS.deposit( 5.58, 0)
  walletUBS.transfer( 5.58, 0.10, 0.8576642335766423, walletCoinbaseEUR)

  # 20.12.2017
  #walletUBSVisa.deposit(234.30, 0)
  #walletUBSVisa.transfer(234.30, ?, ?, walletCoinbaseEUR)

  console.log( "walletUBS", walletUBS.getBalance(), walletUBS.get('currency'))
  console.log( "walletVisa", walletUBS.getBalance(), walletUBS.get('currency'))
  console.log( "walletCoinbaseEUR", walletCoinbaseEUR.getBalance(), walletCoinbaseEUR.get('currency'))
  console.log( "walletCoinbaseLTC", walletCoinbaseLTC.getBalance(), walletCoinbaseLTC.get('currency'))


  # A transaction can only be a deposit or a withdraw.
  # If we want to know from which wallet or to which wallet
  # it goes, it should be in the meta information

  return



  # Creating an account that handles multiple
  # wallets.


  # Creating personal wallets
  kursionWallets = [
    kursionBank = new Wallet( {name: "bankCHF", currency: "CHF", description: "My real bank account"} )
    coinbaseEUR = new Wallet( {name: "coinbaseEUR", currency: "EUR", description: "Coinbase money wallet"} )
  ]

  kursionWallets.forEach( (wallet) -> Log.info wallet.toString() )

  kursionBank.addTransaction( new Transaction(
    type: 'deposit'
    amount: 5.58
    currency: 'CHF'
    fee: 0
  ))
  # TODO: kursionBank.deposit

  coinbaseEUR.deposit(fromWallet, new Transaction(

  ))

  kursionBank.addTransaction( new Transaction(
    type: 'transfer'
    amount: 5.58
    currency: 'CHF'
    fee: 0.10
  ), coinbaseEUR)

  Log.debug "Current kursionBank balance:", kursionBank.getBalance(), "profit:", kursionBank.getProfit()

)

main()




