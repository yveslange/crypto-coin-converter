Bunyan = require('bunyan')
BunyanFormat = require('bunyan-format')
BunyanFormatOut = BunyanFormat({ outputMode: 'short' })

module.exports = Bunyan.createLogger(
  name: "wallet-app"
  streams: [
    {
      level: 'debug',
      stream: BunyanFormatOut
    },
    {
      level: 'info',
      path: 'info.log'
    }
  ]
)
