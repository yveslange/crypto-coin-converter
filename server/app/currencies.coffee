# Converts a currency to another one (including crypto-currencies) on
# a specific exchange.
#
#   Eg: convert MIOTA to BTC on Binance.com exchange.
#
# An exchange might not handle all types of conversion.
# For instance, on Binance.com, there is no MIOTA to USD market. Thus,
# to convert MIOTA to USD, it is needed to do it throught BTC.
#
#   Eg: convert MIOTA to BTC, then BTC to USD.
#
IS = "CURRENCIES>"
Got = require('got')
log = require("./logger")



module.exports = class Currencies
  # The currency being displayed to humains.
  BASE_CUR: 'USD'

  # The crypto-currency used for conversion if the market was not found.
  #
  #   Eg: If market IOTA/USD does not exist, both IOTA/USD and BTC/USD markets
  #       will be used to complete the conversion from IOTA to USD.
  #
  BASE_CRYPTO_CUR: 'BTC'

  # Retrieved currency rates.
  #
  #   Eg: *TODO*
  #
  RATES: {}
  CRYPTO_RATES: {}

  # The time between two updates of the currency rates.
  UPDATER_TIMER: 15*60*1000 # 15 minutes

  # Crypto currencies supported
  CRYPTO_CUR: [ 'USD', 'LTC', 'ETH', 'IOT', 'XMR', 'XRP',
                'XLM', 'ADA', 'SALT', 'TRX' ]

  constructor: ->
    @runUpdater @updateRates
    @runUpdater @updateCryptoRates
    @update()

  update: ->
    Promise.all([
      @updateRates()
      @updateCryptoRates()
    ])


  runUpdater: (func) ->
    setInterval( =>
      func.bind(@)()
    , @UPDATER_TIMER)

  updateRates: ->
    base_cur = @BASE_CUR
    API = "https://api.fixer.io/"
    URL = "#{API}latest?base=#{base_cur}"
    msg = "Updating rates with base_cur=#{base_cur}"
    log.debug IS, msg

    Got(URL, {json: true}).then( (res) =>
      @RATES = res.body.rates
      @RATES[base_cur] = 1
      return @RATES
    ).catch( (err) ->
      log.error IS, msg, "\n\tURL=#{URL} \n\terr=#{err}"
    )

  updateCryptoRates: (exchange="CCCAGG") ->
    crypto_cur = @CRYPTO_CUR.join(',')
    base_crypto_cur = @BASE_CRYPTO_CUR
    API = "https://min-api.cryptocompare.com/data/"
    URL = "#{API}price?fsym=#{base_crypto_cur}&tsyms=#{crypto_cur}"+
          "&tryConversion=true&e=#{exchange}"
    msg = "Updating crypto-rates with base_crypto_cur=#{base_crypto_cur}"
    log.debug IS, msg

    Got(URL, {json: true}).then( (res) =>
      @CRYPTO_RATES = res.body
      @CRYPTO_RATES[base_crypto_cur] = 1
      return @CRYPTO_RATES
    ).catch( (err) ->
      log.error IS, msg, "\n\tURL=#{URL} \n\terr=#{err}"
    )

  convert: (amount, fsym, tsym) ->
    base_cur = @BASE_CUR
    base_crypto_cur = @BASE_CRYPTO_CUR
    log.debug IS, "Converting #{amount} of #{fsym} to #{tsym}"

    # Converting from RATES to RATES
    if fsym of @RATES and tsym of @RATES
      log.debug IS, "Converting from RATES to RATES"
      inCurrencyBase = amount / @RATES[fsym]
      toCurrency = inCurrencyBase * @RATES[tsym]
      return toCurrency

    # Converting from RATES to CRYPTO_RATES
    else if fsym of @RATES and tsym of @CRYPTO_RATES
      log.debug IS, "Converting from RATES to CRYPTO_RATES"
      inCurrencyBase = amount / @RATES[fsym]
      # console.log "#{amount} #{fsym} is #{inCurrencyBase} #{base_cur}"
      inCryptoCurrencyBase = inCurrencyBase / @CRYPTO_RATES[base_cur]
      # console.log "#{inCurrencyBase} #{base_cur} is #{inCryptoCurrencyBase} #{base_crypto_cur}"
      toCurrency = inCryptoCurrencyBase * @CRYPTO_RATES[tsym]
      return toCurrency

    # Converting from CRYPTO_RATES to CRYPTO_RATES
    else if fsym of @CRYPTO_RATES and tsym of @CRYPTO_RATES
      log.debug IS, "Converting from CRYPTO_RATES to CRYPTO_RATES"
      inCryptoCurrencyBase = amount / @CRYPTO_RATES[fsym]
      toCurrency = inCryptoCurrencyBase * @CRYPTO_RATES[tsym]
      return toCurrency

    # Converting from CRYPTO_RATES to RATES
    else if fsym of @CRYPTO_RATES and tsym of @RATES
      log.debug IS, "Converting from CRYPTO_RATES to RATES"
      inCryptoCurrencyBase = amount / @CRYPTO_RATES[fsym]
      # console.log "#{amount} #{fsym} is #{inCryptoCurrencyBase} #{base_crypto_cur}"
      inCurrencyBase = inCryptoCurrencyBase * @CRYPTO_RATES[base_cur]
      # console.log "#{inCryptoCurrencyBase} #{base_crypto_cur} is #{inCurrencyBase} #{base_cur}"
      toCurrency = inCurrencyBase * @RATES[tsym]

    else
      log.debug IS, "Conversion from #{fsym} to #{tsym} not possible"


  info: ->
    return {
      rates: @RATES
      crypto_rates: @CRYPTO_RATES
    }




# MAIN
if require.main == module
  currencies = new Currencies()
  currencies.update().then( ([rate, crypto_rates]) ->
    log.info IS, currencies.info()

    toCur = "USD"
    listCur = ['BTC', 'LTC', 'ETH', 'IOT', 'XMR', 'XRP', 'XLM', 'ADA', 'SALT', 'TRX']
    listCur.forEach( (cur) ->
      result = currencies.convert(1, cur, toCur).toFixed(2)
      log.info IS, "1 #{cur} is #{result} #{toCur}"
    )
  )








